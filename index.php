<?php
include './vendor/autoload.php';

use App\classes\Controller\MemberController;
use App\classes\Controller\ServiceController;
use App\classes\Controller\ReviewController;
use App\classes\Controller\blogController;
use App\classes\Controller\ClientController;
use App\classes\Controller\ContactController;
use App\classes\Model\Format;

$mbrObj = new MemberController;
$serObj = new ServiceController;
$revObj = new ReviewController;
$blogObj = new blogController;
$cliObj = new ClientController;
$conObj = new ContactController;
$format = new Format;
?>
<!DOCTYPE html>
<html lang="en">

    <head>

        <!-- meta -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport"       content="width=device-width, initial-scale=1.0">
        <meta name="description"    content="Nioti - One Page Multipurpose Html Template">
        <meta name="keywords"       content="business, responsive, multipurpose, onepage, corporate, clean">
        <meta name="author"         content="Ambidextrousbd">

        <!-- Site title -->
        <title>EMS::Education Management System</title>

        <!-- favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="assets/img/logo1.png">

        <!-- Bootstrap css -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet">

        <!--Font Awesome css -->
        <link href="assets/css/font-awesome.min.css" rel="stylesheet">

        <!-- Normalizer css -->
        <link href="assets/css/normalize.css" rel="stylesheet">

        <!-- Owl Carousel css -->
        <link href="assets/css/owl.carousel.min.css" rel="stylesheet">
        <link href="assets/css/owl.transitions.css" rel="stylesheet">

        <!-- Magnific popup css -->
        <link href="assets/css/magnific-popup.css" rel="stylesheet">

        <!-- Site css -->
        <link href="assets/css/style.css" rel="stylesheet">

        <!-- Responsive css -->
        <link href="assets/css/responsive.css" rel="stylesheet">



        <!-- <link rel="stylesheet" href="assets/css/skin/orange.css" /> -->
        <!-- <link rel="stylesheet" href="assets/css/skin/green.css" /> -->
        <!-- <link rel="stylesheet" href="assets/css/skin/yellow.css" /> -->
        <!-- <link rel="stylesheet" href="assets/css/skin/wood.css" /> -->
        <!-- <link rel="stylesheet" href="assets/css/skin/asbestos.css" /> -->
        <!-- <link rel="stylesheet" href="assets/css/skin/alizarin.css" /> -->


        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>

        <!-- Preloader starts-->
        <div id="preloader"></div>
        <!-- Preloader ends -->


        <!-- Top Bar Starts -->
        <!--        <div class="top-bar">
                    <div class="container">
                        <div class="tmail">Email: info@unitechengr.com</div>    
                        <div class="tphone">Phone: +880 1618 558 801-10</div>    
                        <div class="tsocial">
                            <a href="#"><i class="fa fa-rss"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-skype"></i></a>
                            <a href="#"><i class="fa fa-google-plus"></i></a>
                            <a href="#"><i class="fa fa-pinterest"></i></a>
                        </div>
                    </div>
                </div>-->
        <!-- Top Bar Ends -->


        <!-- Navigation area starts -->
        <div class="menu-area navbar-fixed-top">
            <div class="container">
                <div class="row">

                    <!-- Navigation starts -->
                    <div class="col-md-12">
                        <div class="mainmenu">
                            <div class="navbar navbar-nobg">
                                <div class="navbar-header">
                                    <a class="navbar-brand" href="index.html"><img src="assets/img/logo1.png" alt=""></a>
                                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                </div>

                                <div class="navbar-collapse collapse">
                                    <nav>
                                        <ul class="nav navbar-nav navbar-right">
                                            <li class="active"><a class="smooth_scroll" href="#slider">HOME</a></li>
                                            <li><a class="smooth_scroll" href="#about">ABOUT</a></li>
                                            <li><a class="smooth_scroll" href="#service">SERVICE</a></li>
                                            <li><a class="smooth_scroll" href="#work">PORTFOLIO</a></li>
                                            <li><a class="smooth_scroll" href="#team">TEAM</a></li>
                                            <li><a class="smooth_scroll" href="#news">NEWS</a></li>
                                            <li><a class="smooth_scroll" href="#contact">CONTACT</a></li>
                                        </ul>
                                    </nav>
                                </div>


                            </div>
                        </div>
                    </div>
                    <!-- Navigation ends -->

                </div>
            </div>
        </div>
        <!-- Navigation area ends -->



        <!-- Slider area starts -->
        <section id="slider">
            <div id="carousel-example-generic" class="carousel slide carousel-fade">

                <div class="carousel-inner" role="listbox">

                    <!-- Item 1 -->
                    <div class="item active slide1">
                        <div class="table">
                            <div class="table-cell">
                                <div class="intro-text">
                                    <!--<img src="assets/img/logo1.png" alt="">-->
                                    <div class="title clearfix">
                                        <h2>Need to grow <br> your campus</h2>
                                        <h1>First</h1>
                                    </div>
                                    <a href="#" class="btn btn-trnsp btn-big">PURCHASE NOW</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Item 2 -->
                    <div class="item slide2">
                        <div class="table">
                            <div class="table-cell">
                                <div class="intro-text">
                                    <img src="assets/img/big-logo.png" alt="">
                                    <div class="title clearfix">
                                        <h2>Need to grow <br> your campus</h2>
                                        <h1>First</h1>
                                    </div>
                                    <a href="#" class="btn btn-trnsp btn-big">PURCHASE NOW</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Item 3 -->
                    <div class="item slide3">
                        <div class="table">
                            <div class="table-cell">
                                <div class="intro-text">
                                    <img src="assets/img/big-logo.png" alt="">
                                    <div class="title clearfix">
                                        <h2>Need to grow <br> your campus</h2>
                                        <h1>First</h1>
                                    </div>
                                    <a href="#" class="btn btn-trnsp btn-big">PURCHASE NOW</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- End Wrapper for slides-->


                <!-- Carousel Pagination -->
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                </ol>


                <!-- Slider left right button -->
                <!-- <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <img src="assets/img/left-arrow.png" alt="">
                </a>
    
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <img src="assets/img/right-arrow.png" alt="">
                </a> -->

            </div>
        </section>
        <!-- Slider area ends -->



        <!-- About area starts -->
        <section id="about" class="about-area section-big">
            <div class="container">

                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="section-title">
                            <h2>About <span>Us</span></h2>
                            <p>A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart.</p>
                        </div>
                    </div>
                </div>

                <div class="row">

                    <!-- About Image -->
                    <div class="col-md-6 about-img">
                        <img src="assets/img/about/about.png" alt="" >
                    </div>

                    <!-- About Text -->
                    <div class="col-md-6">
                        <div class="about-text">
                            <!--<a href="#" class="btn">Read more</a>-->
                            <div class="panel-group" id="accordion" role="tablist">



                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingOne">
                                        <h4 class="panel-title">
                                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-controls="collapseOne">
                                                BENEFITS FOR ADMINISTRATION
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                        <div class="panel-body">
                                            <ul>
                                                <li>Manage Multiple Branches At a Time.</li>
                                                <li>Get Institute Updates Through the Web.</li>
                                                <li>Easy to Works & Maintain Anytime from Anywhere.</li>
                                                <li>Easily Can manage All Approvals.</li>
                                                <li>Barcode System Digital ID Card For tracking information.</li>
                                                <li>Notice Board & Special Corner.</li>
                                                <li>Fees Collection System is Matter Of a Click Now.</li>
                                                <li>Customizable Report Generate System & Many More.</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingTwo">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-controls="collapseTwo">
                                                BENIFITS FOR TEACHERS
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo" aria-expanded="false">
                                        <div class="panel-body">
                                            <ul>
                                                <li>Individual Teacher's Portal.</li>
                                                <li>Can Take Student Attendance Easily.</li>
                                                <li>Exam Mark Entry.</li>
                                                <li>View Lesson Plans.</li>
                                                <li>Send SMS to Parents.</li>
                                                <li>Manage All Academic & More.</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingThree">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-controls="collapseThree">
                                                BENIFITS FOR STUDENTS
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree" aria-expanded="false">
                                        <div class="panel-body">
                                            <ul>
                                                <li>Online Admission Facility.</li>
                                                <li>Students,Parents,Teachers & Administrations Can Communicate
                                                    With Each Other By Sending Text Messages Or E-Mail.
                                                </li>
                                                <li>
                                                    Student Can Get All Of Their Academic Updates through the Student
                                                    Portal/Dashboard Such As: Performanc Report, Personal Information,Class & Exam Routine,Assiignment & Take Etc.
                                                </li>
                                                <li>
                                                    View Lesson Plans.
                                                </li>
                                                <li>
                                                    Send SMS to Parents.
                                                </li>
                                                <li>
                                                    Manage All Academic & More.
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingFour">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-controls="headingFour">
                                                BENIFITS FOR PARENTS
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour" aria-expanded="false">
                                        <div class="panel-body">
                                            <ul>
                                                <li>Parents Portal</li>
                                                <li>Admission ,Attendance, Exam Info, Progress Report , Fee's Collection</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>


                </div>

            </div>
        </section>
        <!-- About area ends -->


        <!-- Service area starts -->
        <section id="service" class="service-area section-big">
            <div class="container">

                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="section-title">
                            <h2>Service We <span>Provide</span></h2>
                            <p>A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart.</p>
                        </div>
                    </div>
                </div>

                <div class="row">

                    <div class="home-services">
                        <!-- Service box -->
                        <?php
                        $result = $serObj->serviceList();
                        if ($result) {
                            foreach ($result as $data) {
                                ?>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <div class="service-box">
                                        <i class="<?php echo $data['icon']; ?>"></i>
                                        <h3 class="subtitle"><?php echo $data['title']; ?></h3>
                                        <p>
                                            <?php echo $data['body']; ?>    
                                        </p>
                                    </div>
                                </div>
                                <?php
                            }
                        }
                        ?>

                    </div>

                </div>

            </div>
        </section>
        <!-- Service area ends -->


        <!-- Work area starts -->
        <section id="work" class="works section-big">
            <div class="container">

                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="section-title">
                            <h2>Our Great <span>Works</span></h2>
                            <p>A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart.</p>
                        </div>
                    </div>
                </div>

                <div class="row">

                    <!-- Works filter -->
                    <ul class="work filters">
                        <li class="filter" data-filter="all">All</li>
                        <li class="filter" data-filter=".dashboard">Dashboard</li>
                        <li class="filter" data-filter=".report">Report</li>
                        <li class="filter" data-filter=".result">Result</li>
                    </ul>
                    <!-- / Works filter -->

                </div>

                <div class="portfolio">
                    <div class="row work-items">

                        <!-- work item -->
                        <div class="col-md-4 col-sm-6 mix dashboard">
                            <div class="item">
                                <a href="assets/img/works/dashboard.jpg" class="work-popup">
                                    <img src="assets/img/works/dashboard.jpg" alt="">
                                    <div class="overlay">
                                        <i class="fa fa-arrows-alt" aria-hidden="true"></i>
                                    </div>
                                </a>
                            </div>
                        </div>

                        <!-- work item -->
                        <div class="col-md-4 col-sm-6 mix report">
                            <div class="item">
                                <a href="assets/img/works/report.jpg" class="work-popup">
                                    <img src="assets/img/works/report.jpg" alt="">
                                    <div class="overlay">
                                        <i class="fa fa-arrows-alt" aria-hidden="true"></i>
                                    </div>
                                </a>
                            </div>
                        </div>

                        <!-- work item -->
                        <div class="col-md-4 col-sm-6 mix result">
                            <div class="item">
                                <a href="assets/img/works/result.jpg" class="work-popup">
                                    <img src="assets/img/works/result.jpg" alt="">
                                    <div class="overlay">
                                        <i class="fa fa-arrows-alt" aria-hidden="true"></i>
                                    </div>
                                </a>
                            </div>
                        </div>

                        <!-- work item -->
                        <div class="col-md-4 col-sm-6 mix result">
                            <div class="item">
                                <a href="assets/img/works/result2.jpg" class="work-popup">
                                    <img src="assets/img/works/result2.jpg" alt="">
                                    <div class="overlay">
                                        <i class="fa fa-arrows-alt" aria-hidden="true"></i>
                                    </div>
                                </a>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </section>
        <!-- Work area ends -->

        <!-- Team area starts -->
        <section id="team" class="team-area section-big">
            <div class="container">


                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="section-title">
                            <h2>CLEVER TEAM <span>MEMBERS</span></h2>
                            <p>A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart.</p>
                        </div>
                    </div>
                </div>

                <div class="row">

                    <?php
                    $result = $mbrObj->getMemberAllData();
                    if ($result) {
                        foreach ($result as $data) {
                            ?>
                            <div class="col-md-3 col-sm-6">
                                <div class="team-member">
                                    <div class="member-image">
                                        <img src="Admin/<?php echo $data['image']; ?>" alt="" style="height: 300px;">
                                    </div>
                                    <div class="member-info">
                                        <h3 class="subtitle"><?php echo $data['name']; ?></h3>
                                        <p class="text-muted"><?php echo $data['designation']; ?></p>
                                        <div class="member-social">
                                            <a href="<?php echo $data['facebook']; ?>"><i class="fa fa-facebook"></i></a>
                                            <a href="<?php echo $data['twitter']; ?>"><i class="fa fa-twitter"></i></a>
                                            <a href="<?php echo $data['gplus']; ?>"><i class="fa fa-google-plus"></i></a>
                                            <a href="<?php echo $data['linkedin']; ?>"><i class="fa fa-linkedin"></i></a>
                                        </div>
                                    </div>
                                    <div class="member-hover">
                                        <h3 class="subtitle"><?php echo $data['name']; ?></h3>
                                        <p class="text-muted"><?php echo $data['designation']; ?></p>
                                        <div class="member-social">
                                            <a href="<?php echo $data['facebook']; ?>"><i class="fa fa-facebook"></i></a>
                                            <a href="<?php echo $data['twitter']; ?>"><i class="fa fa-twitter"></i></a>
                                            <a href="<?php echo $data['gplus']; ?>"><i class="fa fa-google-plus"></i></a>
                                            <a href="<?php echo $data['linkedin']; ?>"><i class="fa fa-linkedin"></i></a>
                                        </div>
                                        <p>
                                            <?php echo $data['body']; ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                    }
                    ?>
                </div>
            </div>
        </section>
        <!-- Team area ends -->


        <!-- fun-facts area starts -->
        <section id="fun-facts" class="fun-facts-area section-big">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-md-3">
                        <div class="fun-fact text-center tab-margin-bottom">
                            <i class="fa fa-thumbs-o-up"></i>
                            <h3><span class="cp-counter">290</span></h3>
                            <p>Projects Completed</p>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="fun-fact text-center tab-margin-bottom">
                            <i class="fa fa-group"></i>
                            <h3> <span class="cp-counter">550</span></h3>
                            <p>Happy Clients</p>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="fun-fact text-center">
                            <i class="fa fa-envelope"></i>
                            <h3> <span class="cp-counter">924</span></h3>
                            <p>Mail Conversation</p>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="fun-fact text-center">
                            <i class="fa fa-file-image-o"></i>
                            <h3> <span class="cp-counter">1656</span></h3>
                            <p>Photos Taken</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- fun-facts area ends -->


        <!-- Testimonial area starts -->
        <section id="testimonial" class="testimonial-area section-big">
            <div class="container">

                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="section-title">
                            <h2>Client <span>Testimonial</span></h2>
                            <p>A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart.</p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="testimonial-list">

                        <!-- Testimonial item -->
                        <?php
                        $data = $revObj->reviewList();

                        if ($data) {
                            foreach ($data as $value) {
                                ?>
                                <div class="single-testimonial">
                                    <!--<img src="assets/img/testimonial/1.jpg" alt="">-->
                                    <i class="fa fa-quote-left"></i>
                                    <p>
                                        <?php echo $value['body']; ?>
                                    </p>
                                    <h4>
                                        <?php echo $value['reviewer']; ?>
                                    </h4>
                                    <p class="desg">
                                        <?php echo $value['company']; ?>
                                    </p>
                                </div>
                                <?php
                            }
                        }
                        ?>

                    </div>
                </div>

            </div>
        </section>
        <!-- Testimonial area ends -->


        <!-- News area starts -->
        <section id="news" class="news-area section-big">
            <div class="container">

                <div class="row">
                    <div class="col-md-12">
                        <div class="section-title text-center">
                            <h2>Our Latest News</h2>
                            <p>A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart.</p>
                        </div>
                    </div>
                </div>

                <div class="row">

                    <!-- News item 1 -->
                    <?php
                    $data = $blogObj->blogList();
                    $i = 0;
                    if ($data) {
                        foreach ($data as $value) {
                            $i++;
                            ?>
                            <div class="col-md-4 col-sm-6">
                                <div class="single-news">
                                    <div class="news-image">
                                        <a href="#news-modal-<?php echo $i; ?>" data-toggle="modal">
                                            <img src="Admin/<?php echo $value['image']; ?>" alt="" >  
                                        </a>
                                    </div>
                                    <div class="news-content">
                                        <a href="#news-modal-<?php echo $i; ?>" data-toggle="modal">
                                            <h3 class="subtitle"><?php echo $value['title']; ?></h3>
                                        </a>
                                        <p class="news-meta text-muted">
                                            <span><i class="fa fa-calendar"></i> 02 Nov 2017 </span>
                                        </p>
                                        <p>
                                            <?php echo substr($value['body'], 0, 100); ?>
                                        </p>
                                        <a class="btn" href="#news-modal-<?php echo $i; ?>" data-toggle="modal">Read More</a>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                    }
                    ?>
                </div>

                <!-- News 1 Modals -->
                <?php
                $data = $blogObj->blogList();
                $i = 0;
                if ($data) {
                    foreach ($data as $value) {
                        $i++;
                        ?>
                        <div class="news-modal modal fade" id="news-modal-<?php echo $i; ?>" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-content">
                                <div class="close-modal" data-dismiss="modal">
                                    <div class="lr">
                                        <div class="rl">
                                        </div>
                                    </div>
                                </div>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-lg-8 col-lg-offset-2">
                                            <div class="modal-body">

                                                <img src="Admin/<?php echo $value['image']; ?>" alt="" >
                                                <h2><?php echo $value['title']; ?></h2>
                                                <p><?php echo $value['body']; ?></p>
                                                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                }
                ?>
            </div>
        </section>
        <!-- News area ends -->


        <!-- Video area starts -->
        <!--        <div id="video" class="video-area section-big">
                    <div class="container text-center">
        
                        <div class="video-content">
                            <a class="popup-youtube" href="http://www.youtube.com/watch?v=xtZE3sMv6lg">
                                <i class="fa fa-play"></i>
                            </a>
                            <h2>Watch This Video</h2>
                        </div>
        
                    </div>
                </div>-->
        <!-- Video area ends -->


        <!-- Client area starts -->
        <section id="client" class="client-area section-big">
            <div class="container">

                <div class="row">
                    <div class="col-md-12">
                        <div class="section-title text-center">
                            <h2>Our Happy <span>Clients</span></h2>
                            <p>A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart.</p>
                        </div>
                    </div>
                </div>

                <!-- client carousel -->
                <div class="owl-client">

                    <!-- item -->
                    <?php
                    $result = $cliObj->clientLogoList();
                    if ($result) {
                        foreach ($result as $data) {
                            ?>
                            <div class="item text-center">
                                <img src="Admin/<?php echo $data['image']; ?>" alt="">
                            </div>
                            <?php
                        }
                    }
                    ?>

                </div>
                <!-- / client carousel -->

            </div>
        </section>
        <!-- Client area ends -->


        <!-- Subscribe area starts -->
        <div id="subscribe" class="subscribe-area section-big">
            <div class="container">
                <div class="subscribe-box">

                    <div class="row">

                        <div class="col-md-6">
                            <h2>SING UP FOR <span>NEWSLETTER</span></h2>
                            <p>We'll send email notifications everytime we release new Theme.</p>                        
                        </div>

                        <div class="col-md-6">
                            <form id="mc-form" class="mc-form">
                                <div class="newsletter-form">

                                    <input type="email" autocomplete="off" id="mc-email" placeholder="Email address" class="form-control">

                                    <button class="mc-submit" type="submit"><i class="fa fa-paper-plane"></i></button>                                

                                    <div class="clearfix"></div>

                                    <!-- mailchimp-alerts Start -->
                                    <div class="mailchimp-alerts">
                                        <div class="mailchimp-submitting"></div><!-- mailchimp-submitting end -->
                                        <div class="mailchimp-success"></div><!-- mailchimp-success end -->
                                        <div class="mailchimp-error"></div><!-- mailchimp-error end -->
                                    </div>
                                    <!-- mailchimp-alerts end -->


                                </div>
                            </form>    
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- Subscribe area ends -->


        <!-- Contact area starts -->
        <section id="contact" class="contact-area section-big">
            <div class="container">

                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="section-title">
                            <h2>GET IN <span>TOUCH</span></h2>
                            <p>
                                Morbi accumsan ipsum velit sodales tellus odio tincidunt auctor ornare sed mauris vitae erat consequat auctor eu in elit class.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="row contact-infos">
                    <div class="col-sm-4 col-xs-12">
                        <div class="contact-info">
                            <i class="fa fa-send"></i>
                            <p>
                                <a href="mailto:info@unitechengr.com" title="Click to mail">info@unitechengr.com</a>
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <div class="contact-info">
                            <i class="fa fa-map-marker"></i>
                            <p>Unitech Engineer's Group, Green Road, Farmgate, Dhaka-1215</p>
                        </div>
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <div class="contact-info">
                            <i class="fa fa-phone" aria-hidden="true"></i>
                            <p>
                                <a href="tel:+8801618558801" title="Click to Call">+8801618558801</a>
                            </p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <?php
//                    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
//                        $message = $conObj->sendMessage($_POST);
//                    }
                    ?>

                    <?php
//                    if (isset($_SESSION['vError'])) {
//                        foreach ($_SESSION['vError'] as $error) {
//                            echo $error . '<br>';
//                        }
//                        unset($_SESSION['vError']);
//                    }
                    ?>
                    <?php
//                    if (isset($message)) {
//                        echo $message;
//                        unset($message);
//                    }
                    ?>
                    <div class="col-md-12">
                        <!-- Contact form starts -->
                        <div class="contact-form">

                            <p id="msg" style="text-align: center;"></p>

                            <form action="" method="post">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <input type="text" name="name" id="name" class="form-control" placeholder="Full Name">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <input type="email" name="email" id="email" class="form-control" placeholder="Email Address">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <input type="text" name="subject" id="subject" class="form-control" placeholder="Subject">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group"> 
                                            <textarea name="body" id="body" rows="5" class="form-control" placeholder="Your Message"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="actions">
                                            <input type="submit" value="Contact ME" class="btn" title="Submit Your Message!">
                                        </div>
                                    </div>
                                </div>
                            </form>

                        </div>
                        <!-- Contact form ends-->
                    </div>
                </div>

            </div>
        </section>
        <!-- Contact area ends -->


        <!-- Google Map starts-->
        <div id="contactgoogleMap"></div>
        <!-- Google Map ends -->



        <!-- Footer area starts -->
        <footer class="footer-area">
            <div class="container text-center">
                <img src="assets/img/logo1.png" alt="">
                <p>Locavore pork belly scenester, pinterest chillwave microdosing waistcoat pop-up. Flexitarian +1 cliche artisan, biodiesel mixtape tacos art party mustache cardigan kitsch squid disrupt.</p>
                <ul class="social-links">
                    <li><a href="#"><i class="fa fa-rss"></i></a></li>
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                    <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                    <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                </ul>
            </div>
        </footer>
        <!-- Footer area ends -->

        <div class="copyright-text text-center">
            <p>&copy; 2017 Copyright Unitechengr</p>
        </div>

        <a href="http://unitechengr.com/ems/" target="_blank" class="scrollToTop" style="display: inline; transition: all 0.5s ease;">
            DEMO
        </a>




        <!-- Latest jQuery -->
        <script src="assets/js/jquery.min.js"></script>

        <!-- Bootstrap js-->
        <script src="assets/js/bootstrap.min.js"></script>

        <!-- Owl Carousel js -->
        <script src="assets/js/owl.carousel.min.js"></script>

        <!-- Mixitup js -->
        <script src="assets/js/jquery.mixitup.js"></script>

        <!-- Magnific popup js -->
        <script src="assets/js/jquery.magnific-popup.min.js"></script>

        <!-- Waypoint js -->
        <script src="assets/js/jquery.waypoints.min.js"></script>

        <!-- Ajax Mailchimp js -->
        <script src="assets/js/jquery.ajaxchimp.min.js"></script>

        <!-- GOOGLE MAP JS -->
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB0YyDTa0qqOjIerob2VTIwo_XVMhrruxo"></script>


        <!-- Main js-->
        <script src="assets/js/main_script.js"></script>
    </body>
</html>
