<?php

include './vendor/autoload.php';

use App\classes\Controller\ContactController;

$conObj = new ContactController;

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $name = $_POST['name'];
    $email = $_POST['email'];
    $subject = $_POST['subject'];
    $body = $_POST['body'];

    echo $conObj->sendMessage($name, $email, $subject, $body);
}
?>