<?php

namespace App\classes\Model;

class Format {

    //Data Filtering
    public function filtering($data = NULL) {
        foreach ($data as $value) {
            $value = trim($value);
            $value = stripslashes($value);
            $value = htmlspecialchars($value);
        }
        return $value;
    }

    //Data Validation
    public function validation($data = NULL) {
        $error = array();
        foreach ($data as $key => $value) {
            if (empty($value)) {
                $error[] = "<span class ='error'>" . ucfirst($key) . " Field Must Not Be Empty.</span>";
            }
        }
        return $error;
    }

    //Text Short
    public function textShort($data, $limit = 60) {
        $text = substr($data, 0, $limit);
        $text = substr($data, 0, strrpos($text, ' ')) . '...'; //use strrpos for prevent word broken
        return $text;
    }

}
