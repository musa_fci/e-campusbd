<?php

namespace App\classes\Model;

use App\classes\Model\Model;

class ServiceModel extends Model {

    //Add Service
    public function addService($data) {
        $query = "INSERT INTO tbl_service(icon,title,body)VALUES('$_POST[icon]','$_POST[title]','$_POST[body]')";
        return $this->db->insert($query);
    }

    //Service List
    public function serviceList() {
        $query = "SELECT * FROM tbl_service ORDER BY service_id ASC";
        return $this->db->select($query);
    }

    //Service List By Id
    public function serviceListById($serviceId) {
        $query = "SELECT * FROM tbl_service WHERE service_id = '$serviceId'";
        return $this->db->select($query);
    }

    //Service Delete
    public function deleteService($delid) {
        $rquery = "DELETE FROM tbl_service WHERE service_id = '$delid'";
        return $this->db->delete($rquery);
    }

    //Service Disable
    public function disableService($disid) {
        $query = "UPDATE tbl_service SET status = '0' WHERE service_id = '$disid'";
        return $this->db->update($query);
    }

    //Service Enable
    public function enableService($enbid) {
        $query = "UPDATE tbl_service SET status = '1' WHERE service_id = '$enbid'";
        return $this->db->update($query);
    }

    //Update Service Info
    public function updateService($data, $serviceId) {
        $query = "UPDATE tbl_service SET icon = '$_POST[icon]',title = '$_POST[title]',body = '$_POST[body]' WHERE service_id = '$serviceId'";
        return $this->db->update($query);
    }

}
