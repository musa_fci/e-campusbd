<?php

namespace App\classes\Model;

use App\classes\Model\Model;

class ClientModel extends Model {

//Add Client Logo
    public function addClientLogo($data) {
        $permit = array("jpg", "jpeg", "png", "gif");
        $file_name = $_FILES['image']['name'];
        $file_size = $_FILES['image']['size'];
        $tmp_name = $_FILES['image']['tmp_name'];

        $div = explode('.', $file_name);
        $file_ext = strtolower(end($div));
        $unique_image = substr(md5(time()), 0, 10) . '.' . $file_ext;
        $uploaded_file = "uploads/cLogo/" . $unique_image;

        if (empty($file_name)) {
            return "<span class='error'>No image select....</span>";
        } elseif ($file_size > 1048567) {
            return "<span class='error'>Picture Size is Too Large....</span>";
        } elseif (in_array($file_ext, $permit) === FALSE) {
            return "<span class='error'>Select Correct Image Format Like As :  " . implode(',', $permit) . "</span>";
        } else {
            move_uploaded_file($tmp_name, $uploaded_file);
            $rquery = "INSERT INTO tbl_client(image)VALUES('$uploaded_file')";
            return $this->db->insert($rquery);
        }
    }

    //Client Logo List
    public function clientLogoList() {
        $query = "SELECT * FROM tbl_client";
        return $this->db->select($query);
    }

    //Client Logo Delete
    public function clientLogoDelete($delid) {
        $query = "SELECT * FROM tbl_client WHERE client_id = '$delid'";
        if ($result = $this->db->select($query)) {
            foreach ($result as $data) {
                $image = $data['image'];
                unlink($image);
            }
        }

        $rquery = "DELETE FROM tbl_client WHERE client_id = '$delid'";
        return $this->db->delete($rquery);
    }

}
