<?php

namespace App\classes\Model;

use App\classes\Model\Model;

class ContactModel extends Model {

    //Send Message
    public function sendMessage($name, $email, $subject, $body) {
        $query = "INSERT INTO tbl_contact(name,email,subject,body)VALUES('$name','$email','$subject','$body')";
        return $this->db->insert($query);
    }

    //Message List
    public function messageList() {
        $query = "SELECT * FROM tbl_contact";
        return $this->db->select($query);
    }

    //Message Delete
    public function messageDelete($delid) {
        $query = "DELETE FROM tbl_contact WHERE id = '$delid'";
        return $this->db->delete($query);
    }

}
