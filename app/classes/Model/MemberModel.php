<?php

namespace App\classes\Model;

use App\classes\Model\Model;

class MemberModel extends Model {

    //Add Member
    public function addMember($data) {
        $query = "SELECT * FROM tbl_members WHERE email = '$_POST[email]'";
        if ($this->db->select($query)) {
            return "<span class='error'>User Already Exists....</span>";
        } else {
            $permit = array("jpg", "jpeg", "png", "gif");
            $file_name = $_FILES['image']['name'];
            $file_size = $_FILES['image']['size'];
            $tmp_name = $_FILES['image']['tmp_name'];

            $div = explode('.', $file_name);
            $file_ext = strtolower(end($div));
            $unique_image = substr(md5(time()), 0, 10) . '.' . $file_ext;
            $uploaded_file = "uploads/" . $unique_image;

            if (empty($file_name)) {
                return "<span class='error'>No image select....</span>";
            } elseif ($file_size > 1048567) {
                return "<span class='error'>Picture Size is Too Large....</span>";
            } elseif (in_array($file_ext, $permit) === FALSE) {
                return "<span class='error'>Select Correct Image Format Like As :  " . implode(',', $permit) . "</span>";
            } else {
                move_uploaded_file($tmp_name, $uploaded_file);
                $rquery = "INSERT INTO tbl_members(name,email,image,designation,body)VALUES('$_POST[name]','$_POST[email]','$uploaded_file','$_POST[designation]','$_POST[body]')";
                return $this->db->insert($rquery);
            }
        }
    }

    //Member List
    public function memberList() {
        $query = "SELECT * FROM tbl_members";
        return $this->db->select($query);
    }

    //Member List By Id
    public function memberListById($memberId) {
        $query = "SELECT * FROM tbl_members WHERE member_id = '$memberId'";
        return $this->db->select($query);
    }

    //Category Delete
    public function deleteMember($delid) {
        $query = "SELECT * FROM tbl_members WHERE member_id = '$delid'";
        if ($result = $this->db->select($query)) {
            foreach ($result as $data) {
                $image = $data['image'];
                unlink($image);
            }
        }

        $rquery = "DELETE FROM tbl_members WHERE member_id = '$delid'";
        return $this->db->delete($rquery);
    }

    //Category Disable
    public function disableMember($disid) {
        $query = "UPDATE tbl_members SET status = '0' WHERE member_id = '$disid'";
        return $this->db->update($query);
    }

    //Category Enable
    public function enableMember($enbid) {
        $query = "UPDATE tbl_members SET status = '1' WHERE member_id = '$enbid'";
        return $this->db->update($query);
    }

    //Update Member Info
    public function updateMember($data, $memberId) {
        if (!empty($_FILES['image']['name'])) {

            $query = "SELECT * FROM tbl_members WHERE member_id = '$memberId'";
            $result = $this->db->select($query);
            if ($result) {
                foreach ($result as $data) {
                    $image = $data['image'];
                    unlink($image);
                }
            }

            $permit = array("jpg", "jpeg", "png", "gif");
            $file_name = $_FILES['image']['name'];
            $file_size = $_FILES['image']['size'];
            $tmp_name = $_FILES['image']['tmp_name'];

            $div = explode('.', $file_name);
            $file_ext = strtolower(end($div));
            $unique_image = substr(md5(time()), 0, 10) . '.' . $file_ext;
            $uploaded_file = "uploads/" . $unique_image;

            if (empty($file_name)) {
                return "<span class='error'>No image select....</span>";
            } elseif ($file_size > 1048567) {
                return "<span class='error'>Picture Size is Too Large....</span>";
            } elseif (in_array($file_ext, $permit) === FALSE) {
                return "<span class='error'>Select Correct Image Format Like As :  " . implode(',', $permit) . "</span>";
            } else {
                move_uploaded_file($tmp_name, $uploaded_file);
                $query = "UPDATE tbl_members SET name = '$_POST[name]',email = '$_POST[email]',image = '$uploaded_file',designation = '$_POST[designation]',body = '$_POST[body]' WHERE member_id = '$memberId'";
                return $this->db->update($query);
            }
        } else {
            $query = "UPDATE tbl_members SET name = '$_POST[name]',email = '$_POST[email]',designation = '$_POST[designation]',body = '$_POST[body]' WHERE member_id = '$memberId'";
            return $this->db->update($query);
        }
    }

    //Add Social
    public function addSocial($memberId, $data) {
        $query = "SELECT * FROM tbl_social WHERE member_id = '$memberId'";
        if ($this->db->select($query)) {
            return "<span class='error'>Member Social Already Added....</span>";
        } else {
            $query = "INSERT INTO tbl_social(member_id,facebook,twitter,gplus,linkedin)VALUES('$memberId','$_POST[facebook]','$_POST[twitter]','$_POST[gplus]','$_POST[linkedin]')";
            return $this->db->insert($query);
        }
    }

    //Member Ways Social List
    public function memberWaysSocialList($memberId) {
        $query = "SELECT * FROM tbl_social WHERE member_id = '$memberId'";
        return $this->db->select($query);
    }

    //Member Ways Social Delete
    public function memberWaysSocialDelete($delid) {
        $query = "DELETE FROM tbl_social WHERE social_id = '$delid'";
        return $this->db->delete($query);
    }

    //Member Ways Social Update
    public function memberWaysSocialUpdate($memberId, $data) {
        $query = "UPDATE tbl_social SET facebook = '$_POST[facebook]',twitter = '$_POST[twitter]',gplus = '$_POST[gplus]',linkedin = '$_POST[linkedin]' WHERE member_id = '$memberId'";
        return $this->db->update($query);
    }

    //Get Member All Data
    public function getMemberAllData() {
        $query = "SELECT m.*,s.* FROM tbl_members as m,tbl_social as s WHERE m.member_id = s.member_id AND m.status = 1 ORDER BY m.member_id ASC";
        return $this->db->select($query);
    }

}
