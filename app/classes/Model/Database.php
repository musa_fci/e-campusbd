<?php

namespace App\classes\Model;

use App\fixed\Fixed;

$fixedObj = new Fixed;

class Database {

    private $host = HOST;
    private $user = USER;
    private $pass = PASS;
    private $database = DB;
    private $db;

    public function __construct() {
        $this->db = $this->connect();
    }

    public function connect() {
        $con = new \mysqli($this->host, $this->user, $this->pass, $this->database);
        if ($con->connect_error) {
            echo "Connection Fail" . $con->connect_error;
        } else {
            return $con;
        }
    }

    //Select data
    public function select($query) {
        $result = $this->db->query($query);
        if ($result->num_rows > 0) {
            return $result;
        } else {
            return FALSE;
        }
    }

    //Insert data
    public function insert($query) {
        $insert = $this->db->query($query);
        if ($insert) {
            return "<span class='success'>Data Insert Successfully....</span>";
        } else {
            return "<span class='error'>Data Not Inserted....</span>" . $this->db->error;
        }
    }

    // Update data
    public function update($query) {
        $update = $this->db->query($query);
        if ($update) {
            return "<span class='success'>Data Update Successfully....</span>";
        } else {
            return "<span class='error'>Data Not Updated....</span>" . $this->db->error;
        }
    }

    //Delete data
    public function delete($query) {
        $delete = $this->db->query($query);
        if ($delete) {
            return "<span class='success'>Data Delete Successfully....</span>";
        } else {
            return "<span class='error'>Data Not Deleted....</span>" . $this->db->error;
        }
    }

}
