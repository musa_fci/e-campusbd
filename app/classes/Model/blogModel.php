<?php

namespace App\classes\Model;

use App\classes\Model\Model;

class blogModel extends Model {

    //Add Blog
    public function addBlog($data) {
        $permit = array("jpg", "jpeg", "png", "gif");
        $file_name = $_FILES['image']['name'];
        $file_size = $_FILES['image']['size'];
        $tmp_name = $_FILES['image']['tmp_name'];

        $div = explode('.', $file_name);
        $file_ext = strtolower(end($div));
        $unique_image = substr(md5(time()), 0, 10) . '.' . $file_ext;
        $uploaded_file = "uploads/blog/" . $unique_image;

        if (empty($file_name)) {
            return "<span class='error'>No image select....</span>";
        } elseif ($file_size > 1048567) {
            return "<span class='error'>Picture Size is Too Large....</span>";
        } elseif (in_array($file_ext, $permit) === FALSE) {
            return "<span class='error'>Select Correct Image Format Like As :  " . implode(',', $permit) . "</span>";
        } else {
            move_uploaded_file($tmp_name, $uploaded_file);
            $rquery = "INSERT INTO tbl_blog(image,title,body)VALUES('$uploaded_file','$_POST[title]','$_POST[body]')";
            return $this->db->insert($rquery);
        }
    }

    //Blog List
    public function blogList() {
        $query = "SELECT * FROM tbl_blog";
        return $this->db->select($query);
    }

    //Blog List By Id
    public function blogListById($blogId) {
        $query = "SELECT * FROM tbl_blog WHERE blog_id = '$blogId'";
        return $this->db->select($query);
    }

    //Blog Delete
    public function deleteBlog($delid) {
        $query = "SELECT * FROM tbl_blog WHERE blog_id = '$delid'";
        if ($result = $this->db->select($query)) {
            foreach ($result as $data) {
                $image = $data['image'];
                unlink($image);
            }
        }

        $rquery = "DELETE FROM tbl_blog WHERE blog_id = '$delid'";
        return $this->db->delete($rquery);
    }

    //Blog Disable
    public function disableBlog($disid) {
        $query = "UPDATE tbl_blog SET status = '0' WHERE blog_id = '$disid'";
        return $this->db->update($query);
    }

    //Blog Enable
    public function enableBlog($enbid) {
        $query = "UPDATE tbl_blog SET status = '1' WHERE blog_id = '$enbid'";
        return $this->db->update($query);
    }

    //Update Blog Info
    public function updateBlog($data, $blogId) {
        if (!empty($_FILES['image']['name'])) {

            $query = "SELECT * FROM tbl_blog WHERE blog_id = '$blogId'";
            $result = $this->db->select($query);
            if ($result) {
                foreach ($result as $data) {
                    $image = $data['image'];
                    unlink($image);
                }
            }

            $permit = array("jpg", "jpeg", "png", "gif");
            $file_name = $_FILES['image']['name'];
            $file_size = $_FILES['image']['size'];
            $tmp_name = $_FILES['image']['tmp_name'];

            $div = explode('.', $file_name);
            $file_ext = strtolower(end($div));
            $unique_image = substr(md5(time()), 0, 10) . '.' . $file_ext;
            $uploaded_file = "uploads/blog/" . $unique_image;

            if (empty($file_name)) {
                return "<span class='error'>No image select....</span>";
            } elseif ($file_size > 1048567) {
                return "<span class='error'>Picture Size is Too Large....</span>";
            } elseif (in_array($file_ext, $permit) === FALSE) {
                return "<span class='error'>Select Correct Image Format Like As :  " . implode(',', $permit) . "</span>";
            } else {
                move_uploaded_file($tmp_name, $uploaded_file);
                $query = "UPDATE tbl_blog SET image = '$uploaded_file',title = '$_POST[title]',body = '$_POST[body]' WHERE blog_id = '$blogId'";
                return $this->db->update($query);
            }
        } else {
            $query = "UPDATE tbl_blog SET title = '$_POST[title]',body = '$_POST[body]' WHERE blog_id = '$blogId'";
            return $this->db->update($query);
        }
    }

}
