<?php

namespace App\classes\Model;

use App\classes\Model\Model;

class ReviewModel extends Model {

    //Add Review
    public function addReview($data) {
        $query = "INSERT INTO tbl_review(body,reviewer,company)VALUES('$_POST[body]','$_POST[reviewer]','$_POST[company]')";
        return $this->db->insert($query);
    }

    //Review List
    public function reviewList() {
        $query = "SELECT * FROM tbl_review ORDER BY review_id ASC";
        return $this->db->select($query);
    }

    //Review List By Id
    public function reviewListById($reviewId) {
        $query = "SELECT * FROM tbl_review WHERE review_id = '$reviewId'";
        return $this->db->select($query);
    }

    //Review Delete
    public function deleteReview($delid) {
        $rquery = "DELETE FROM tbl_review WHERE review_id = '$delid'";
        return $this->db->delete($rquery);
    }

    //Review Disable
    public function disableReview($disid) {
        $query = "UPDATE tbl_review SET status = '0' WHERE review_id = '$disid'";
        return $this->db->update($query);
    }

    //Review Enable
    public function enableReview($enbid) {
        $query = "UPDATE tbl_review SET status = '1' WHERE review_id = '$enbid'";
        return $this->db->update($query);
    }

    //Update Review Info
    public function updateReview($data, $reviewId) {
        $query = "UPDATE tbl_review SET body = '$_POST[body]',reviewer = '$_POST[reviewer]',company = '$_POST[company]' WHERE review_id = '$reviewId'";
        return $this->db->update($query);
    }

}
