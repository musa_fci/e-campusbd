<?php

namespace App\classes\Controller;

use App\classes\Model\AdminModel;
use App\classes\Model\Format;

class AdminController {

    protected $model;
    protected $format;

    public function __construct() {
        $this->model = new AdminModel();
        $this->format = new Format();
    }

    public function adminLogin($data) {
        $fData = $this->format->filtering($data);
        $vData = $this->format->validation($data);
        if (count($vData) > 0) {
            $_SESSION['vError'] = $vData;
        } else {
            return $this->model->adminLogin($fData);
        }
    }

}
