<?php

namespace App\classes\Controller;

use App\classes\Model\ServiceModel;
use App\classes\Model\Format;

class ServiceController {

    protected $model;
    protected $format;

    public function __construct() {
        $this->model = new ServiceModel();
        $this->format = new Format();
    }

    //Add Service
    public function addService($data) {
        $fData = $this->format->filtering($data);
        $vData = $this->format->validation($data);
        if (count($vData) > 0) {
            $_SESSION['vError'] = $vData;
        } else {
            return $this->model->addService($fData);
        }
    }

    //Service List
    public function serviceList() {
        return $this->model->serviceList();
    }

    //Service List By Id
    public function serviceListById($serviceId) {
        return $this->model->serviceListById($serviceId);
    }

    //Service Delete
    public function deleteService($delid) {
        return $this->model->deleteService($delid);
    }

    //Service Disable
    public function disableService($disid) {
        return $this->model->disableService($disid);
    }

    //Service Enable
    public function enableService($enbid) {
        return $this->model->enableService($enbid);
    }

    //Update Service Info
    public function updateService($data, $serviceId) {
        $fData = $this->format->filtering($data);
        $vData = $this->format->validation($data);
        if (count($vData) > 0) {
            $_SESSION['vError'] = $vData;
        } else {
            return $this->model->updateService($fData, $serviceId);
        }
    }

}
