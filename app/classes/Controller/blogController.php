<?php

namespace App\classes\Controller;

use App\classes\Model\blogModel;
use App\classes\Model\Format;

class blogController {

    protected $model;
    protected $format;

    public function __construct() {
        $this->model = new blogModel();
        $this->format = new Format();
    }

    //Add Blog
    public function addBlog($data) {
        $fData = $this->format->filtering($data);
        $vData = $this->format->validation($data);
        if (count($vData) > 0) {
            $_SESSION['vError'] = $vData;
        } else {
            return $this->model->addBlog($fData);
        }
    }

    //Blog List
    public function blogList() {
        return $this->model->blogList();
    }

    //Blog List By Id
    public function blogListById($blogId) {
        return $this->model->blogListById($blogId);
    }

    //Blog Delete
    public function deleteBlog($delid) {
        return $this->model->deleteBlog($delid);
    }

    //Blog Disable
    public function disableBlog($disid) {
        return $this->model->disableBlog($disid);
    }

    //Blog Enable
    public function enableBlog($enbid) {
        return $this->model->enableBlog($enbid);
    }

    //Update Blog Info
    public function updateBlog($data, $blogId) {
        $fData = $this->format->filtering($data);
        $vData = $this->format->validation($data);
        if (count($vData) > 0) {
            $_SESSION['vError'] = $vData;
        } else {
            return $this->model->updateBlog($fData, $blogId);
        }
    }

}
