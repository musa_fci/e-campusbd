<?php

namespace App\classes\Controller;

use App\classes\Model\ClientModel;
use App\classes\Model\Format;

class ClientController {

    protected $model;
    protected $format;

    public function __construct() {
        $this->model = new ClientModel();
        $this->format = new Format();
    }

    //Add Client Logo
    public function addClientLogo($data) {
        return $this->model->addClientLogo($data);
    }

    //Client Logo List
    public function clientLogoList() {
        return $this->model->clientLogoList();
    }

    //Client Logo Delete
    public function clientLogoDelete($delid) {
        return $this->model->clientLogoDelete($delid);
    }

}
