<?php

namespace App\classes\Controller;

use App\classes\Model\ContactModel;
use App\classes\Model\Format;

class ContactController {

    protected $model;
    protected $format;

    public function __construct() {
        $this->model = new ContactModel();
        $this->format = new Format();
    }

    //Send Message
    public function sendMessage($name, $email, $subject, $body) {
        return $this->model->sendMessage($name, $email, $subject, $body);
    }

    //Message List
    public function messageList() {
        return $this->model->messageList();
    }

    //Message Delete
    public function messageDelete($delid) {
        return $this->model->messageDelete($delid);
    }

}
