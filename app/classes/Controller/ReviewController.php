<?php

namespace App\classes\Controller;

use App\classes\Model\ReviewModel;
use App\classes\Model\Format;

class ReviewController {

    protected $model;
    protected $format;

    public function __construct() {
        $this->model = new ReviewModel();
        $this->format = new Format();
    }

    //Add Review
    public function addReview($data) {
        $fData = $this->format->filtering($data);
        $vData = $this->format->validation($data);
        if (count($vData) > 0) {
            $_SESSION['vError'] = $vData;
        } else {
            return $this->model->addReview($fData);
        }
    }

    //Review List
    public function reviewList() {
        return $this->model->reviewList();
    }

    //Review List By Id
    public function reviewListById($reviewId) {
        return $this->model->reviewListById($reviewId);
    }

    //Review Delete
    public function deleteReview($delid) {
        return $this->model->deleteReview($delid);
    }

    //Review Disable
    public function disableReview($disid) {
        return $this->model->disableReview($disid);
    }

    //Review Enable
    public function enableReview($enbid) {
        return $this->model->enableReview($enbid);
    }

    //Update Review Info
    public function updateReview($data, $reviewId) {
        $fData = $this->format->filtering($data);
        $vData = $this->format->validation($data);
        if (count($vData) > 0) {
            $_SESSION['vError'] = $vData;
        } else {
            return $this->model->updateReview($fData, $reviewId);
        }
    }

}
