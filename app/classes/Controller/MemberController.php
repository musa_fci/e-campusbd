<?php

namespace App\classes\Controller;

use App\classes\Model\MemberModel;
use App\classes\Model\Format;

class MemberController {

    protected $model;
    protected $format;

    public function __construct() {
        $this->model = new MemberModel();
        $this->format = new Format();
    }

    //Add Member
    public function addMember($data) {
        $fData = $this->format->filtering($data);
        $vData = $this->format->validation($data);
        if (count($vData) > 0) {
            $_SESSION['vError'] = $vData;
        } else {
            return $this->model->addMember($fData);
        }
    }

    //Member List
    public function memberList() {
        return $this->model->memberList();
    }

    //Member List By Id
    public function memberListById($memberId) {
        return $this->model->memberListById($memberId);
    }

    //Member Delete
    public function deleteMember($delid) {
        return $this->model->deleteMember($delid);
    }

    //Member Disable
    public function disableMember($disid) {
        return $this->model->disableMember($disid);
    }

    //Member Enable
    public function enableMember($enbid) {
        return $this->model->enableMember($enbid);
    }

    //Update Member Info
    public function updateMember($data, $memberId) {
        $fData = $this->format->filtering($data);
        $vData = $this->format->validation($data);
        if (count($vData) > 0) {
            $_SESSION['vError'] = $vData;
        } else {
            return $this->model->updateMember($fData, $memberId);
        }
    }

    //Add Member Ways Social
    public function addSocial($memberId, $data) {
        $fData = $this->format->filtering($data);
        $vData = $this->format->validation($data);
        if (count($vData) > 0) {
            $_SESSION['vError'] = $vData;
        } else {
            return $this->model->addSocial($memberId, $fData);
        }
    }

    //Member Ways Social List
    public function memberWaysSocialList($memberId) {
        return $this->model->memberWaysSocialList($memberId);
    }

    //Member Ways Social Delete
    public function memberWaysSocialDelete($delid) {
        return $this->model->memberWaysSocialDelete($delid);
    }

    //Member Ways Social Update
    public function memberWaysSocialUpdate($memberId, $data) {
        $fData = $this->format->filtering($data);
        $vData = $this->format->validation($data);
        if (count($vData) > 0) {
            $_SESSION['vError'] = $vData;
        } else {
            return $this->model->memberWaysSocialUpdate($memberId, $fData);
        }
    }

    //Get Member All Data
    public function getMemberAllData() {
        return $this->model->getMemberAllData();
    }

}
