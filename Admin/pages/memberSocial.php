<?php
if (isset($_GET['memberId'])) {
    $memberId = $_GET['memberId'];
}
?>

<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $addsocial = $mbrObj->addSocial($memberId, $_POST);
}
?>

<?php
//Member Ways Social Media Delete
if (isset($_GET['delid'])) {
    $delid = $_GET['delid'];
    $socialDelete = $mbrObj->memberWaysSocialDelete($delid);

    if ($socialDelete) {
        echo "<script>setTimeout(function() {window.location = '?page=memberSocial&memberId=$memberId'},1000);</script>";
    }
}
?>


<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>
        <a href="index.php">Home</a>
        <i class="icon-angle-right"></i> 
    </li>
    <li>
        <i class="icon-edit"></i>
        <a href="#">Social</a>
    </li>
</ul>

<div class="row-fluid sortable">
    <?php
    if (isset($_SESSION['vError'])) {
        foreach ($_SESSION['vError'] as $error) {
            echo $error . '<br>';
        }
        unset($_SESSION['vError']);
    }
    ?>

    <?php
    if (isset($addsocial)) {
        echo $addsocial;
        unset($addsocial);
    }
    ?>

    <?php
    if (isset($socialDelete)) {
        echo $socialDelete;
        unset($socialDelete);
    }
    ?>
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon edit"></i><span class="break"></span>Add Social</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
                <fieldset>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Facebook</label>
                        <div class="controls">
                            <input type="text" name="facebook" class="span6 typeahead" id="typeahead">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Twitter</label>
                        <div class="controls">
                            <input type="text" name="twitter" class="span6 typeahead" id="typeahead">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Google Plus</label>
                        <div class="controls">
                            <input type="text" name="gplus" class="span6 typeahead" id="typeahead">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Linkedin</label>
                        <div class="controls">
                            <input type="text" name="linkedin" class="span6 typeahead" id="typeahead">
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Add Social List</button>
                        <button type="reset" class="btn">Cancel</button>
                    </div>
                </fieldset>
            </form> 
        </div>
    </div><!--/span-->



    <div>
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon user"></i><span class="break"></span>Member Ways Social Media List</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th width="5%">No.</th>
                        <th width="20%">Facebook</th>
                        <th width="20%">Twitter</th>
                        <th width="20%">Google Plus</th>
                        <th width="20%">Linkedin</th>
                        <th width="30%">Action</th>
                    </tr>
                </thead>   
                <tbody>
                    <?php
                    $sociallist = $mbrObj->memberWaysSocialList($memberId);
                    $i = 0;
                    if ($sociallist) {
                        foreach ($sociallist as $value) {
                            $i++;
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td class="center"><?php echo $value['facebook']; ?></td>
                                <td class="center"><?php echo $value['twitter']; ?></td>
                                <td class="center"><?php echo $value['gplus']; ?></td>
                                <td class="center"><?php echo $value['linkedin']; ?></td>
                                <td class="center">
                                    <a class = "btn btn-primary" href = "?page=memberSocialUpdate&&memberId=<?php echo $memberId; ?>">
                                        Edit
                                    </a>
                                    <a class = "btn btn-danger" onclick = "return confirm('Are You Sure to Delete......!')" href = "?page=memberSocial&&memberId=<?php echo $memberId; ?>&&delid=<?php echo $value['social_id']; ?>">
                                        Delete
                                    </a>
                                </td>

                            </tr>
                            <?php
                        }
                    }
                    ?>
                </tbody>
            </table>            
        </div>
    </div>

</div><!--/row-->

