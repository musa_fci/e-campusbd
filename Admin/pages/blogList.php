<?php
//Blog Delete
if (isset($_GET['delid'])) {
    $delid = $_GET['delid'];
    $blogDelete = $blogObj->deleteBlog($delid);

    if ($blogDelete) {
        echo "<script>setTimeout(function() {window.location = '?page=blogList'},1000);</script>";
    }
}
?>

<?php
//Blog Disable
if (isset($_GET['disid'])) {
    $disid = $_GET['disid'];
    $blogDisable = $blogObj->disableBlog($disid);
    if ($blogDisable) {
        echo "<script>window.location = '?page=blogList'</script>";
    }
}
?>

<?php
//Blog Enable
if (isset($_GET['enbid'])) {
    $enbid = $_GET['enbid'];
    $blogEnable = $blogObj->enableBlog($enbid);
    if ($blogEnable) {
        echo "<script>window.location = '?page=blogList'</script>";
    }
}
?>


<div class="row-fluid sortable">

    <?php
    //Blog Delete Message
    if (isset($blogDelete)) {
        echo $blogDelete;
        unset($blogDelete);
    }
    ?>

    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon user"></i><span class="break"></span>Blog List</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th width="5%">No.</th>
                        <th width="20%">Image</th>
                        <th width="20%">Title</th>
                        <th width="40%">Body</th>
                        <th width="5%">Status</th>
                        <th width="10%">Action</th>
                    </tr>
                </thead>   
                <tbody>
                    <?php
                    $data = $blogObj->blogList();
                    $i = 0;
                    if ($data) {
                        foreach ($data as $value) {
                            $i++;
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td class="center">
                                    <img src="<?php echo $value['image']; ?>">
                                </td>
                                <td class="center"><?php echo $value['title']; ?></td>
                                <td class="center"><?php echo $value['body']; ?></td>
                                <td class="center">
                                    <?php
                                    if ($value['status'] == 1) {
                                        ?>
                                        <span class="label label-success">Active</span>
                                        <?php
                                    } else {
                                        ?>
                                        <span class="label label-danger">Inactive</span>
                                    <?php } ?>
                                </td>

                                <td class="center">
                                    <?php
                                    if ($value['status'] == 1) {
                                        ?>
                                        <a class = "btn btn-success btn_width" href = "?page=blogList&disid=<?php echo $value['blog_id']; ?>">
                                            Disable
                                        </a>
                                        <?php
                                    } else {
                                        ?>
                                        <a class = "btn btn-primary btn_width" href = "?page=blogList&enbid=<?php echo $value['blog_id']; ?>">
                                            Enable
                                        </a>

                                    <?php } ?>

                                    <a class = "btn btn-info btn_width" href = "?page=blogUpdate&blogId=<?php echo $value['blog_id']; ?>">
                                        Edit
                                    </a>
                                    <a class = "btn btn-danger btn_width" onclick = "return confirm('Are You Sure to Delete......!')" href = "?page=blogList&delid=<?php echo $value['blog_id']; ?>">
                                        Delete
                                    </a>
                                </td>

                            </tr>
                            <?php
                        }
                    }
                    ?>
                </tbody>
            </table>            
        </div>
    </div>
</div>