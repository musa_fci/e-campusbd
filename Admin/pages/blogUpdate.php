<?php
if (isset($_GET['blogId'])) {
    $blogId = $_GET['blogId'];
}
?>

<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $updateblog = $blogObj->updateBlog($_POST, $blogId);
}
?>


<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>
        <a href="index.php">Home</a>
        <i class="icon-angle-right"></i> 
    </li>
    <li>
        <i class="icon-edit"></i>
        <a href="#">Blog Update</a>
    </li>
</ul>

<div class="row-fluid sortable">
    <?php
    if (isset($_SESSION['vError'])) {
        foreach ($_SESSION['vError'] as $error) {
            echo $error . '<br>';
        }
        unset($_SESSION['vError']);
    }
    ?>

    <?php
    if (isset($updateblog)) {
        echo $updateblog;
        unset($updateblog);
    }
    ?>
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon edit"></i><span class="break"></span>Update Blog Info</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <?php
            $result = $blogObj->blogListById($blogId);
            if ($result) {
                foreach ($result as $data) {
                    ?>
                    <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
                        <fieldset>
                            <div class="control-group">
                                <div class="controls">
                                    <img src="<?php echo $data['image']; ?>" width="200px" height="200px">
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label" for="typeahead">Image</label>
                                <div class="controls">
                                    <input type="file" name="image" class="span6 typeahead" id="typeahead">
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label" for="typeahead">Title</label>
                                <div class="controls">
                                    <input type="text" value="<?php echo $data['title']; ?>" name="title" class="span6 typeahead" id="typeahead">
                                </div>
                            </div>

                            <div class="control-group hidden-phone">
                                <label class="control-label" for="textarea2">Blog</label>
                                <div class="controls">
                                    <textarea name="body" class="cleditor" id="textarea2" rows="3">
                                        <?php echo $data['body']; ?>
                                    </textarea>
                                </div>
                            </div>

                            <div class="form-actions">
                                <button type="submit" class="btn btn-primary">Update Blog Info</button>
                                <button type="reset" class="btn">Cancel</button>
                            </div>
                        </fieldset>
                    </form> 
                    <?php
                }
            }
            ?>
        </div>
    </div><!--/span-->

</div><!--/row-->

