<?php
//Message Delete
if (isset($_GET['delid'])) {
    $delid = $_GET['delid'];
    $msgDelete = $conObj->messageDelete($delid);

    if ($msgDelete) {
        echo "<script>setTimeout(function() {window.location = '?page=messageList'},1000);</script>";
    }
}
?>


<div class="row-fluid sortable">
    <?php
    if (isset($msgDelete)) {
        echo $msgDelete;
        unset($msgDelete);
    }
    ?>
    <div>
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon user"></i><span class="break"></span>Messages List</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th width="5%">No.</th>
                        <th width="20%">Name</th>
                        <th width="20%">Email</th>
                        <th width="20%">Subject</th>
                        <th width="20%">Body</th>
                        <th width="20%">Action</th>
                    </tr>
                </thead>   
                <tbody>
                    <?php
                    $msglist = $conObj->messageList();
                    $i = 0;
                    if ($msglist) {
                        foreach ($msglist as $value) {
                            $i++;
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td class="center">
                                    <?php echo $value['name']; ?>
                                </td>

                                <td class="center">
                                    <?php echo $value['email']; ?>
                                </td>

                                <td class="center">
                                    <?php echo $value['subject']; ?>
                                </td>

                                <td class="center">
                                    <?php echo $value['body']; ?>
                                </td>

                                <td class="center">
                                    <a class = "btn btn-danger" onclick = "return confirm('Are You Sure to Delete......!')" href = "?page=messageList&delid=<?php echo $value['id']; ?>">
                                        Delete
                                    </a>
                                </td>

                            </tr>
                            <?php
                        }
                    }
                    ?>
                </tbody>
            </table>            
        </div>
    </div>

</div>