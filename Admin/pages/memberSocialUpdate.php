<?php
if (isset($_GET['memberId'])) {
    $memberId = $_GET['memberId'];
}
?>

<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $socialUpdate = $mbrObj->memberWaysSocialUpdate($memberId, $_POST);
}
?>


<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>
        <a href="index.php">Home</a>
        <i class="icon-angle-right"></i> 
    </li>
    <li>
        <i class="icon-edit"></i>
        <a href="#">Social Update</a>
    </li>
</ul>

<div class="row-fluid sortable">
    <?php
    if (isset($_SESSION['vError'])) {
        foreach ($_SESSION['vError'] as $error) {
            echo $error . '<br>';
        }
        unset($_SESSION['vError']);
    }
    ?>

    <?php
    if (isset($socialUpdate)) {
        echo $socialUpdate;
        unset($socialUpdate);
    }
    ?>
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon edit"></i><span class="break"></span>Update Social</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <?php
            $result = $mbrObj->memberWaysSocialList($memberId);
            if ($result) {
                foreach ($result as $data) {
                    ?>
                    <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
                        <fieldset>
                            <div class="control-group">
                                <label class="control-label" for="typeahead">Facebook</label>
                                <div class="controls">
                                    <input type="text" value="<?php echo $data['facebook']; ?>" name="facebook" class="span6 typeahead" id="typeahead">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="typeahead">Twitter</label>
                                <div class="controls">
                                    <input type="text" value="<?php echo $data['twitter']; ?>" name="twitter" class="span6 typeahead" id="typeahead">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="typeahead">Google Plus</label>
                                <div class="controls">
                                    <input type="text" value="<?php echo $data['gplus']; ?>" name="gplus" class="span6 typeahead" id="typeahead">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="typeahead">Linkedin</label>
                                <div class="controls">
                                    <input type="text" value="<?php echo $data['linkedin']; ?>" name="linkedin" class="span6 typeahead" id="typeahead">
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-primary">Update Social List</button>
                                <button type="reset" class="btn">Cancel</button>
                            </div>
                        </fieldset>
                    </form> 
                    <?php
                }
            }
            ?>
        </div>
    </div><!--/span-->

</div><!--/row-->

