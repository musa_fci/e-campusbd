<?php
//Member Delete
if (isset($_GET['delid'])) {
    $delid = $_GET['delid'];
    $serviceDelete = $serObj->deleteService($delid);

    if ($serviceDelete) {
        echo "<script>setTimeout(function() {window.location = '?page=serviceList'},1000);</script>";
    }
}
?>

<?php
//Member Disable
if (isset($_GET['disid'])) {
    $disid = $_GET['disid'];
    $serviceDisable = $serObj->disableService($disid);
    if ($serviceDisable) {
        echo "<script>window.location = '?page=serviceList'</script>";
    }
}
?>

<?php
//Member Enable
if (isset($_GET['enbid'])) {
    $enbid = $_GET['enbid'];
    $serviceEnable = $serObj->enableService($enbid);
    if ($serviceEnable) {
        echo "<script>window.location = '?page=serviceList'</script>";
    }
}
?>


<div class="row-fluid sortable">

    <?php
    //Service Delete Message
    if (isset($serviceDelete)) {
        echo $serviceDelete;
        unset($serviceDelete);
    }
    ?>

    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon user"></i><span class="break"></span>Service List</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th width="5%">No.</th>
                        <th width="20%">Icon</th>
                        <th width="10%">Title</th>
                        <th width="40%">Body</th>
                        <th width="10%">Status</th>
                        <th width="10%">Action</th>
                    </tr>
                </thead>   
                <tbody>
                    <?php
                    $data = $serObj->serviceList();
                    $i = 0;
                    if ($data) {
                        foreach ($data as $value) {
                            $i++;
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td class="center"><?php echo $value['icon']; ?></td>
                                <td class="center"><?php echo $value['title']; ?></td>
                                <td class="center"><?php echo $value['body']; ?></td>

                                <td class="center">
                                    <?php
                                    if ($value['status'] == 1) {
                                        ?>
                                        <span class="label label-success">Active</span>
                                        <?php
                                    } else {
                                        ?>
                                        <span class="label label-danger">Inactive</span>
                                    <?php } ?>
                                </td>

                                <td class="center">
                                    <?php
                                    if ($value['status'] == 1) {
                                        ?>
                                        <a class = "btn btn-success btn_width" href = "?page=serviceList&disid=<?php echo $value['service_id']; ?>">
                                            Disable
                                        </a>
                                        <?php
                                    } else {
                                        ?>
                                        <a class = "btn btn-primary btn_width" href = "?page=serviceList&enbid=<?php echo $value['service_id']; ?>">
                                            Enable
                                        </a>

                                    <?php } ?>

                                    <a class = "btn btn-info btn_width" href = "?page=serviceUpdate&serviceId=<?php echo $value['service_id']; ?>">
                                        Edit
                                    </a>
                                    <a class = "btn btn-danger btn_width" onclick = "return confirm('Are You Sure to Delete......!')" href = "?page=serviceList&delid=<?php echo $value['service_id']; ?>">
                                        Delete
                                    </a>
                                </td>

                            </tr>
                            <?php
                        }
                    }
                    ?>
                </tbody>
            </table>            
        </div>
    </div>
</div>