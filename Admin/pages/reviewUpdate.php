<?php
if (isset($_GET['reviewId'])) {
    $reviewId = $_GET['reviewId'];
}
?>
<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $updatereview = $revObj->updateReview($_POST, $reviewId);
}
?>


<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>
        <a href="index.php">Home</a>
        <i class="icon-angle-right"></i> 
    </li>
    <li>
        <i class="icon-edit"></i>
        <a href="#">Review Update</a>
    </li>
</ul>

<div class="row-fluid sortable">
    <?php
    if (isset($_SESSION['vError'])) {
        foreach ($_SESSION['vError'] as $error) {
            echo $error . '<br>';
        }
        unset($_SESSION['vError']);
    }
    ?>

    <?php
    if (isset($updatereview)) {
        echo $updatereview;
        unset($updatereview);
    }
    ?>
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon edit"></i><span class="break"></span>Update Review Info</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <?php
            $result = $revObj->reviewListById($reviewId);
            if ($result) {
                foreach ($result as $data) {
                    ?>
                    <form action="" method="post" class="form-horizontal">
                        <fieldset>
                            <div class="control-group hidden-phone">
                                <label class="control-label" for="textarea2">Description</label>
                                <div class="controls">
                                    <textarea name="body" class="cleditor" id="textarea2" rows="3">
                                        <?php echo $data['body']; ?>
                                    </textarea>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="typeahead">Reviewer</label>
                                <div class="controls">
                                    <input type="text" name="reviewer" value="<?php echo $data['reviewer']; ?>" class="span6 typeahead" id="typeahead">
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label" for="typeahead">Company</label>
                                <div class="controls">
                                    <input type="text" name="company" value="<?php echo $data['company']; ?>" class="span6 typeahead" id="typeahead">
                                </div>
                            </div>



                            <div class="form-actions">
                                <button type="submit" class="btn btn-primary">Update Review</button>
                                <button type="reset" class="btn">Cancel</button>
                            </div>
                        </fieldset>
                    </form> 
                    <?php
                }
            }
            ?>
        </div>
    </div><!--/span-->

</div><!--/row-->

