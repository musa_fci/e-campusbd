<?php
//Review Delete
if (isset($_GET['delid'])) {
    $delid = $_GET['delid'];
    $reviewDelete = $revObj->deleteReview($delid);

    if ($reviewDelete) {
        echo "<script>setTimeout(function() {window.location = '?page=reviewList'},1000);</script>";
    }
}
?>

<?php
//Review Disable
if (isset($_GET['disid'])) {
    $disid = $_GET['disid'];
    $reviewDisable = $revObj->disableReview($disid);
    if ($reviewDisable) {
        echo "<script>window.location = '?page=reviewList'</script>";
    }
}
?>

<?php
//Review Enable
if (isset($_GET['enbid'])) {
    $enbid = $_GET['enbid'];
    $reviewEnable = $revObj->enableReview($enbid);
    if ($reviewEnable) {
        echo "<script>window.location = '?page=reviewList'</script>";
    }
}
?>


<div class="row-fluid sortable">

    <?php
    //Review Delete Message
    if (isset($reviewDelete)) {
        echo $reviewDelete;
        unset($reviewDelete);
    }
    ?>

    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon user"></i><span class="break"></span>Review List</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th width="5%">No.</th>
                        <th width="40%">Body</th>
                        <th width="20%">Reviewer</th>
                        <th width="20%">Company</th>
                        <th width="10%">Status</th>
                        <th width="10%">Action</th>
                    </tr>
                </thead>   
                <tbody>
                    <?php
                    $data = $revObj->reviewList();
                    $i = 0;
                    if ($data) {
                        foreach ($data as $value) {
                            $i++;
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td class="center"><?php echo $value['body']; ?></td>
                                <td class="center"><?php echo $value['reviewer']; ?></td>
                                <td class="center"><?php echo $value['company']; ?></td>

                                <td class="center">
                                    <?php
                                    if ($value['status'] == 1) {
                                        ?>
                                        <span class="label label-success">Active</span>
                                        <?php
                                    } else {
                                        ?>
                                        <span class="label label-danger">Inactive</span>
                                    <?php } ?>
                                </td>

                                <td class="center">
                                    <?php
                                    if ($value['status'] == 1) {
                                        ?>
                                        <a class = "btn btn-success btn_width" href = "?page=reviewList&disid=<?php echo $value['review_id']; ?>">
                                            Disable
                                        </a>
                                        <?php
                                    } else {
                                        ?>
                                        <a class = "btn btn-primary btn_width" href = "?page=reviewList&enbid=<?php echo $value['review_id']; ?>">
                                            Enable
                                        </a>

                                    <?php } ?>

                                    <a class = "btn btn-info btn_width" href = "?page=reviewUpdate&reviewId=<?php echo $value['review_id']; ?>">
                                        Edit
                                    </a>
                                    <a class = "btn btn-danger btn_width" onclick = "return confirm('Are You Sure to Delete......!')" href = "?page=reviewList&delid=<?php echo $value['review_id']; ?>">
                                        Delete
                                    </a>
                                </td>

                            </tr>
                            <?php
                        }
                    }
                    ?>
                </tbody>
            </table>            
        </div>
    </div>
</div>