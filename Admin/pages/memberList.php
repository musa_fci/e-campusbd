<?php
//Member Delete
if (isset($_GET['delid'])) {
    $delid = $_GET['delid'];
    $memberDelete = $mbrObj->deleteMember($delid);

    if ($memberDelete) {
        echo "<script>setTimeout(function() {window.location = '?page=memberList'},1000);</script>";
    }
}
?>

<?php
//Member Disable
if (isset($_GET['disid'])) {
    $disid = $_GET['disid'];
    $memberDisable = $mbrObj->disableMember($disid);
    if ($memberDisable) {
        echo "<script>window.location = '?page=memberList'</script>";
    }
}
?>

<?php
//Member Enable
if (isset($_GET['enbid'])) {
    $enbid = $_GET['enbid'];
    $memberEnable = $mbrObj->enableMember($enbid);

    if ($memberEnable) {
        echo "<script>window.location = '?page=memberList'</script>";
    }
}
?>


<div class="row-fluid sortable">

    <?php
    //Member Delete Message
    if (isset($memberDelete)) {
        echo $memberDelete;
        unset($memberDelete);
    }
    ?>

    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon user"></i><span class="break"></span>Member List</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th width="5%">No.</th>
                        <th width="20%">Name</th>
                        <th width="20%">Email</th>
                        <th width="10%">Image</th>
                        <th width="5%">Designation</th>
                        <th width="20%">Body</th>
                        <th width="5%">Social</th>
                        <th width="5%">Status</th>
                        <th width="10%">Action</th>
                    </tr>
                </thead>   
                <tbody>
                    <?php
                    $data = $mbrObj->memberList();
                    $i = 0;
                    if ($data) {
                        foreach ($data as $value) {
                            $i++;
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td class="center"><?php echo $value['name']; ?></td>
                                <td class="center"><?php echo $value['email']; ?></td>
                                <td class="center">
                                    <img src="<?php echo $value['image']; ?>">
                                </td>
                                <td class="center"><?php echo $value['designation']; ?></td>
                                <td class="center"><?php echo $format->textShort($value['body'], 30); ?></td>
                                <td>
                                    <a href="?page=memberSocial&memberId=<?php echo $value['member_id']; ?>" class="btn btn-success">
                                        social
                                    </a>
                                </td>
                                <td class="center">
                                    <?php
                                    if ($value['status'] == 1) {
                                        ?>
                                        <span class="label label-success">Active</span>
                                        <?php
                                    } else {
                                        ?>
                                        <span class="label label-danger">Inactive</span>
                                    <?php } ?>
                                </td>

                                <td class="center">
                                    <?php
                                    if ($value['status'] == 1) {
                                        ?>
                                        <a class = "btn btn-success btn_width" href = "?page=memberList&disid=<?php echo $value['member_id']; ?>">
                                            Disable
                                        </a>
                                        <?php
                                    } else {
                                        ?>
                                        <a class = "btn btn-primary btn_width" href = "?page=memberList&enbid=<?php echo $value['member_id']; ?>">
                                            Enable
                                        </a>

                                    <?php } ?>

                                    <a class = "btn btn-info btn_width" href = "?page=memberUpdate&memberId=<?php echo $value['member_id']; ?>">
                                        Edit
                                    </a>
                                    <a class = "btn btn-danger btn_width" onclick = "return confirm('Are You Sure to Delete......!')" href = "?page=memberList&delid=<?php echo $value['member_id']; ?>">
                                        Delete
                                    </a>
                                </td>

                            </tr>
                            <?php
                        }
                    }
                    ?>
                </tbody>
            </table>            
        </div>
    </div>
</div>